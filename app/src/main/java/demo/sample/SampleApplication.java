package demo.sample;

import android.app.Application;
import android.content.Context;

import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.internal.RxBleLog;
import com.polidea.rxandroidble.mockrxandroidble.RxBleClientMock;

import java.util.UUID;

import rx.subjects.PublishSubject;

public class SampleApplication extends Application {

    //SET UP MOCK-UP DEVICE
    UUID serviceUUID = UUID.fromString("00001234-0000-0000-8000-000000000000");
    UUID characteristicUUID = UUID.fromString("00002a29-0000-1000-8000-00805f9b34fb");
    UUID characteristicNotifiedUUID = UUID.fromString("00002a29-0000-1000-8000-00805f9b34fb");
    byte[] characteristicData = "Hello phone".getBytes();
    UUID descriptorUUID = UUID.fromString("00001337-0000-1000-8000-00805f9b34fb");
    byte[] descriptorData = "Get 'Hello pin'".getBytes();
    PublishSubject characteristicNotificationSubject = PublishSubject.create();
    private RxBleClient rxBleClient;

    public static RxBleClient getRxBleClient(Context context) {
        SampleApplication application = (SampleApplication) context.getApplicationContext();

        return application.rxBleClient;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        RxBleDevice bleDevice = new RxBleClientMock.DeviceBuilder()
                .deviceMacAddress("AA:BB:CC:DD:EE:FF")
                .deviceName("TestDevice")
                .scanRecord("ScanRecord".getBytes())
                .rssi(42)
                .addService(
                        serviceUUID,
                        new RxBleClientMock.CharacteristicsBuilder()
                                .addCharacteristic(
                                        characteristicUUID,
                                        characteristicData,
                                        new RxBleClientMock.DescriptorsBuilder()
                                                .addDescriptor(descriptorUUID, descriptorData)
                                                .build()
                                ).build()
                )
                .notificationSource(characteristicNotifiedUUID, characteristicNotificationSubject)
                .build();
        rxBleClient = new RxBleClientMock.Builder()
                .addDevice(bleDevice).build();

        RxBleClient.setLogLevel(RxBleLog.DEBUG);
    }
}

