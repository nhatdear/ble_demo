package com.example.nhat.myapplication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.polidea.rxandroidble.RxBleClient;
import com.polidea.rxandroidble.RxBleConnection;
import com.polidea.rxandroidble.RxBleDevice;
import com.polidea.rxandroidble.mockrxandroidble.RxBleClientMock;
import com.polidea.rxandroidble.utils.ConnectionSharingAdapter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.UUID;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.observers.TestSubscriber;
import rx.subjects.PublishSubject;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    protected RxBleClient rxBleClient;
    protected Observable<RxBleConnection> connectionObservable;
    protected PublishSubject<Void> disconnectTriggerSubject = PublishSubject.create();
    UUID serviceUUID = UUID.fromString("00001234-0000-0000-8000-000000000000");
    UUID characteristicUUID = UUID.fromString("00002a29-0000-1000-8000-00805f9b34fb");
    UUID characteristicNotifiedUUID = UUID.fromString("00002a29-0000-1000-8000-00805f9b34fb");
    byte[] characteristicData = "Hello phone".getBytes();
    UUID descriptorUUID = UUID.fromString("00001337-0000-1000-8000-00805f9b34fb");
    byte[] descriptorData = "Get 'Hello pin'".getBytes();
    PublishSubject characteristicNotificationSubject = PublishSubject.create();

    @Before
    public void setUp(){
        RxBleDevice bleDevice = new RxBleClientMock.DeviceBuilder()
                .deviceMacAddress("AA:BB:CC:DD:EE:FF")
                .deviceName("TestDevice")
                .scanRecord("ScanRecord".getBytes())
                .rssi(42)
                .addService(
                        serviceUUID,
                        new RxBleClientMock.CharacteristicsBuilder()
                                .addCharacteristic(
                                        characteristicUUID,
                                        characteristicData,
                                        new RxBleClientMock.DescriptorsBuilder()
                                                .addDescriptor(descriptorUUID, descriptorData)
                                                .build()
                                ).build()
                )
                .notificationSource(characteristicNotifiedUUID, characteristicNotificationSubject)
                .build();
        rxBleClient = new RxBleClientMock.Builder()
                .addDevice(bleDevice).build();
        Context appContext = InstrumentationRegistry.getTargetContext();
        connectionObservable = bleDevice
                .establishConnection(appContext, false)
                .takeUntil(disconnectTriggerSubject)
                .compose(new ConnectionSharingAdapter());
    }
    @Test
    public void sendText() throws Exception {
        connectionObservable
                .flatMap(rxBleConnection -> rxBleConnection.writeCharacteristic(characteristicUUID, "Hello pin!".getBytes()))
                .observeOn(AndroidSchedulers.mainThread())
                .map( data -> new String(data))
                .subscribe(stringValue -> {
                    Log.d("JUNIT --- ",stringValue);
                    assertEquals("Hello phone!",stringValue);
                });

        connectionObservable
                .flatMap(rxBleConnection -> rxBleConnection.writeCharacteristic(characteristicUUID, "Any key!".getBytes()))
                .observeOn(AndroidSchedulers.mainThread())
                .map( data -> new String(data))
                .subscribe(stringValue -> {
                    Log.d("JUNIT --- ",stringValue);
                    assertEquals("Wrong key! Please type 'Hello pin!'",stringValue);
                });
    }

    @After
    public void tearDown() {
        connectionObservable = null;
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.example.nhat.myapplication", appContext.getPackageName());
    }
}
