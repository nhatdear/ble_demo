**# README #**

- - - -
# *** HOW TO TEST PROJECT #
## ** Method 1 ##
1. Install RxAndroidBleSample.APK (on apk folder or build by Android Studio , remember unlock unknown source restriction) into real Device from Android 4.4W to 6.0
2. Open the app
3.  
    * At first activity, press start scan to scan mocked device
    * Click mocked device, you will move to second activity - Device Activity
4. Click at Service Discovery button, you will move to Service Discovery Activity
5. Click at "Connect & Discover Services", you will see the services and characteristics of mocked device
6. Click at characteristic : "Get 'Hello pin!'" , you will go to Characteristic Operations
7. In the text box, write key and "Send Text" to mocked device 
    - If you send "Hello pin!", mocked device reply "Hello phone!"
    - If you send others, mocked device reply "Wrong key"

## ** Method 2 ##
1. Get source code from Git : [https://bitbucket.org/nhatdear/ble_demo](Link URL)
2. Open it by Android Studio
3. Run "ExampleInstrumentedTest" as UnitTest
4. You will see "Hello phone!" in console.

- - - -
# *** TIMESHEET #
#|Tasks  | Usage ( hour )
------------- |------------- | -------------
1| Create mocked device             |  1   
2| Create sample UI ( inherit RxAndroidBle library sample )                |  2   
3| Send text to mocked device       |  1   
4| Mock result when send text to mocked device    |  3
5| Implement Unit Test with JUNIT    |  0.5
|| **7.5**